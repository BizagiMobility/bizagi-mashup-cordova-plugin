var session = window.sessionStorage.getItem("bizagiAuthentication") || "{}";
var BIZAGI_ENABLE_OFFLINE_FORMS = true;
session = JSON.parse(session);

var BIZAGI_DEFAULT_CURRENCY_INFO = {
  "symbol": session.symbol || "$",
  "decimalSeparator": session.decimalSeparator || ",",
  "groupSeparator": session.groupSeparator || ".",
  "decimalDigits": session.decimalDigits || "2"
};

var BIZAGI_DEFAULT_DATETIME_INFO = {
  "shortDateFormat": session.shortDateFormat || "dd/MM/yyyy",
  "timeFormat": session.timeFormat || "H:mm",
  "longDateFormat": session.longDateFormat || "dddd, dd' de 'MMMM' de 'yyyy"
};

var BIZAGI_SETTINGS = {
  "UploadMaxFileSize":  bizagiConfig.uploadMaxFileSize || "1048576"
};

var BIZAGI_USER_PREFERENCES_PAGE = '';
//  var BIZAGI_SESSION_NAME = "ASP.NET_SessionId";

//activation service
var BIZAGI_SERVICE_ACTIVATION = true ;

// Gets the loader instance, and load the module
var loader = bizagi.loader;

loader.preInit(["bizagiDefault", bizagiConfig.environment, undefined, "./"], [
  bizagiConfig.defaultLanguage || session.language || "en", bizagiConfig.log || false, bizagi.override.Inbox_RowsPerPage || "",
  [session.symbol || "$", session.decimalSeparator || ",", session.groupSeparator || ".", session.decimalDigits || "2"],
  [session.shortDateFormat || "dd/MM/yyyy", session.timeFormat || "H:mm", session.longDateFormat || "dddd, dd' de 'MMMM' de 'yyyy"],
  [session.uploadMaxFileSize || bizagiConfig.uploadMaxFileSize || "1048576"], "",
  "ASP.NET_SessionId"
]);

/**
* Allows normalize url
* @param url
* @returns {*}
*/
var mashupUtil = {
  normalizeUrl: function (url) {
    if (url) {
      if (url[url.length - 1] !== '/') {
        url = url.concat('/');
      }

      if (url.indexOf('://') >= 0) {
        url = url.substring(0, url.indexOf('://')).toLowerCase() + url.slice(url.indexOf('://'));
      }

      if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
        url = 'http://' + url;
      }
    }
    return url;
  },
  IsJsonString: function(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }
};

/*======== LOGICA DE MODULO MASHUP ========*/
var mashup = {
  initialized : false,
  mashupInstance: {}
};

loader.init({
  url: "jquery/bizagi.module.definition.json.txt",
  callback: function() {
    console.log("Bizagi definition loaded");

    Object.defineProperties(mashup, {
      "proxyPrefix": { get: function () { return bizagiConfig.proxyPrefix; } },
      "proxyPrefix": { set: function (server) { bizagiConfig.proxyPrefix = mashupUtil.normalizeUrl(server); } }
    });

    mashup.initModule = function(params){
      var self = this;
      params = params || {};

      self.proxyPrefix = params.proxyPrefix || "";
      self.authentication = params.authentication || {};
      self.authentication.type = self.authentication.type || "OAuth";

      //first check the server is valid
      var request = new XMLHttpRequest();
      request.open('GET', bizagiConfig.proxyPrefix + "Rest/Authentication/BizagiConfig", true);

      request.onreadystatechange = function() {
        if(request.readyState === 4) { // done
          if (request.status === 200 && mashupUtil.IsJsonString(request.responseText)){
            if (params.success){
              params.success("success");
            }
          }
          else{
            if (params.error){
              params.error("There is something wrong with your url server");
            }
          }
        }
      }

      request.send(null);
    };

    mashup.authenticateUser = function(params){
      var self = this;

      if (self.authentication.type == "OAuth"){
        //Everything is ok
        loader.start("mashup").then(function() {
          bizagi.enableCustomizations = true;

          //clean all the body
          if (mashup.initialized){
            mashup.mashupInstance.dataService.serviceLocator.setProxyPrefix({proxyPrefix: bizagiConfig.proxyPrefix, context: "workportal"});
            mashup.mashupInstance.controller.publish("refreshProxyPrefix", {proxyPrefix: bizagiConfig.proxyPrefix, context: "workflow"});
            mashup.mashupInstance.authenticateUser({"accessToken": params.accessToken}).then(function(data){
              if (params.success){
                params.success(mashup.mashupInstance);
              }
            });
          }
          else{
            mashup.mashupInstance = new bizagi.mashup.facade({
              proxyPrefix: bizagiConfig.proxyPrefix,
              authentication: self.authentication,
              onClose: function () {
                var mashup_close = new CustomEvent("close", {
                  detail: {
                  },
                  bubbles: true,
                  cancelable: true
                });

                document.dispatchEvent(mashup_close);
              },
              onError: function(e){
                var mashup_error = new CustomEvent("mashup-error", {
                  detail: {
                    error: e
                  },
                  bubbles: true,
                  cancelable: true
                });

                document.dispatchEvent(mashup_error);

              },
              onRenderAction: function (e) {
                var mashup_renderAction = new CustomEvent("render-action", {
                  detail: {
                    action: e
                  },
                  bubbles: true,
                  cancelable: true
                });

                document.dispatchEvent(mashup_renderAction);
              },
              showHeader: true
            });

            mashup.initialized = true;

            var mashup_ready = new CustomEvent("mashupReady", {
              detail: {
                instance: mashup.mashupInstance
              },
              bubbles: true,
              cancelable: true
            });

            mashup.mashupInstance.authenticateUser({"accessToken": params.accessToken}).then(function(data){
              document.dispatchEvent(mashup_ready);

              if (params.success){
                params.success(mashup.mashupInstance);
              }
            });
          }
        });
      }else if (self.authentication.type == "Basic")
      {
        //Not implemented yet
      }
      else{
        var data = {
          'user': params.user.username,
          'password': params.user.password,
          'domain': params.user.domain,
          'loginOption' : 'AlwaysAsk'
        };
        self.user = data;

        //Everything is ok
        loader.start("mashup").then(function() {
          bizagi.enableCustomizations = true;

          //clean all the body
          if (mashup.initialized){
            mashup.mashupInstance.dataService.serviceLocator.setProxyPrefix({proxyPrefix: bizagiConfig.proxyPrefix, context: "workportal"});
            mashup.mashupInstance.controller.publish("refreshProxyPrefix", {proxyPrefix: bizagiConfig.proxyPrefix, context: "workflow"});
            mashup.mashupInstance.authenticateUser(data).then(function(data){
              if (params.success){
                params.success(mashup.mashupInstance);
              }
            });
          }
          else{
            mashup.mashupInstance = new bizagi.mashup.facade({
              proxyPrefix: bizagiConfig.proxyPrefix,
              authentication: self.authentication,
              onClose: function () {
                var mashup_close = new CustomEvent("close", {
                  detail: {
                  },
                  bubbles: true,
                  cancelable: true
                });

                document.dispatchEvent(mashup_close);
              },
              onError: function(e){
                var mashup_error = new CustomEvent("mashup-error", {
                  detail: {
                    error: e
                  },
                  bubbles: true,
                  cancelable: true
                });

                document.dispatchEvent(mashup_error);

              },
              onRenderAction: function (e) {
                var mashup_renderAction = new CustomEvent("render-action", {
                  detail: {
                    action: e
                  },
                  bubbles: true,
                  cancelable: true
                });

                document.dispatchEvent(mashup_renderAction);
              },
              showHeader: true
            });

            mashup.initialized = true;

            var mashup_ready = new CustomEvent("mashupReady", {
              detail: {
                instance: mashup.mashupInstance
              },
              bubbles: true,
              cancelable: true
            });

            mashup.mashupInstance.authenticateUser(data).then(function(){
              document.dispatchEvent(mashup_ready);

              if (params.success){
                params.success(mashup.mashupInstance);
              }
            });
          }
        });
      }
    };

    var mashup_ready = new CustomEvent("ready", {
      detail: {},
      bubbles: true,
      cancelable: true
    });
    document.dispatchEvent(mashup_ready);
  }

});
