# Bizagi mashups
Bizagi's cordova plugin SDK, allows you to embed your Bizagi's forms inside your mobile application. The forms within your application will work exactly as they do in the official Bizagi app.

_**Note:** Cordova supported version 6.3.1 and above._

## Important considerations

1. Our Cordova demo app was developed using Polymer, but it could have been developed using any framework like Angular, Kendo, etc. There are no restrictions.
2. The plugin does not include the display of a user's inbox. It expects to receive a case number and an optional workitem id to resolve the form that will be presented to the end user. That is, the plugin displays exclusively a Bizagi activity form that belong to a process, given a Case number and an optional Workitem id. Thus, developers need to evaluate if an inbox with pending activities is needed or not.
3. Consider using OData or the SOA layer to collect all the information required to build whichever presentation method desired. In our demo we built the user's Inbox. For more information refer to Bizagi's OData at [OData services](http://help.bizagi.com/bpm-suite/en/index.html?api_odata.htm) or Bizagi's SOA layer at [Bizagi from external applications](http://help.bizagi.com/bpmsuite/en/index.html?bizagi_web_services.htm).
4. The plugin doesn't save user credentials.

To enable the use of Bizagi within your application, the first step is to install the Bizagi mashup Cordova plugin, provided in the demo package.

In your application scope execute the following command:
```bash
>> cordova plugin add https://BizagiMobility@bitbucket.org/BizagiMobility/bizagi-mashup-cordova-plugin.git
```
OR

```bash
>> cordova plugin add C:\download-path\bizagi-mashup-cordova-plugin
```

_Where **C:\download-path\** is the local path to the package files._

## Bizagi Cordova Demo App

To configure the provided demo app, you must have installed node, bower and polymer-cli.

Please go to the code folder in the project "mashupDemo\www" and execute the following commands:
```bash
>> npm install -g bower polymer-cli
>> bower install
```

After that it's necessary to add the platform where you want to deploy the app.

IOs:
```bash
>> cordova platform add ios
>> cordova prepare ios
>> cordova run ios
```
Android:
```bash
>> cordova platform add android
>> cordova prepare android
>> cordova run android
```

Finally you could deploy the app and the result should be as following.

![custom Bizagi inbox](../img/inbox.jpg "Inbox in a Cordova demo application")

## Bizagi mashup initialization

Before you can paint forms you need to initialize the module and define configuration data by running the following methods in order.
1. Initialization
2. Set Bizagi server url
3. Set user data - Required for Bizagi authentication, Optional for OAuth authentication.
4. Authenticate user

Then you will be able to paint the Bizagi's forms or execute other available methods at any time.

**Initialization sample**

```javascript
// When deviceready...
document.addEventListener('deviceready', function(e){
  mashup.initialize({
    canvas: document.getElementsByTagName("body")[0],
    showHeader: true,
    authentication:{
       type: "Oauth",
       refreshTokenCallback: function(success, error){ // Just necessary for OAuth Authentication
        // @TODO - refresh the token and send it back to mashups module
        // success(access_token);
       }
    },
    onInitSuccess: function(){
      // @TODO
    },
    onClose: function () {
      // @TODO
    },
    onError: function(e){
      // @TODO
    },
    onRenderAction: function (e) {
      // @TODO
    }
  });
}
```

**Set Bizagi server url data sample**

At this point it is necessary to define the path of you Bizagi web instance


```javascript
mashup.server = "http://path-url/my-project/";
```

**Set user data sample**

At this point it is necessary to define the Bizagi user credentials, if it is OAuth authentication this step is optional

```javascript
mashup.user = {
  username: "username",
  password: "password",
  domain: "domain"
});
```

**Authentication sample**

***OAuth authentication***

```javascript
mashup.authenticateUser({
  access_token: "xyz", // Required for OAuth authentication
  success: function(instance){
    // @TODO
  },
  error: function(error){
    // @TODO
  }
});
```

***Bizagi authentication***

```javascript
mashup.authenticateUser({
  success: function(instance){
    // @TODO
  },
  error: function(error){
    // @TODO
  }
});
```

**Full example for Bizagi authentication**

```javascript
document.addEventListener('deviceready', function(e){
  var canvas = $("body");
  mashup.initialize({
    canvas: canvas,
    showHeader: true,
    authentication: {
      type:"Bizagi"
    },
    onInitSuccess: function(){

      //Set Bizagi server
      mashup.server = "http://url/my-project/";
      //Set user credentials, just for Bizagi authentication
      mashup.user = {
        username: "username",
        password: "password",
        domain: "domain",
      };

      // Try to authenticate the user and creates the mashup instance
      mashup.authenticateUser({
        success: function(instance){
          console.log("success");
        },
        error: function(error){
          console.log("error");
        }
      });
    },
    onClose: function () {
      console.log("closed");
    },
    onError: function(e){
      console.log("error");
    },
    onRenderAction: function (e) {
      console.log("actions");
    }
  });

}, false);
```

**Full example for OAuth authentication**

```javascript
document.addEventListener('deviceready', function(e){
  var canvas = $("body");

  mashup.initialize({
    canvas: canvas,
    showHeader: true,
    authentication:{
      "Oauth",
      refreshTokenCallback: function(success, error){
        // @TODO - refresh the token and send it back to mashups module
        // Delegates the refresh of the token to the client app and helps
        // you to keep the access token synchronized all the time
        success(access_token);
      }
    },
    onInitSuccess: function(){

      //Set Bizagi server
      mashup.server = "http://url/my-project/";

      // Try to authenticate the user and creates the mashup instance
      mashup.authenticateUser({
        access_token: "xyz",
        success: function(instance){
          console.log("success");
        },
        error: function(error){
          console.log("error");
        }
      });
    },
    onClose: function () {
      console.log("closed");
    },
    onError: function(e){
      console.log("error");
    },
    onRenderAction: function (e) {
      console.log("actions");
    }
  });

}, false);
```

## Render forms

Once the mashup module is initialized it is possible to call the render method, or create a new case.

```javascript
mashup.renderCase({"idCase": 558});
```
OR
```javascript
mashup.renderCase({"idCase": 558, "idworkitem": 800});
```
OR

```javascript
mashup.createNewCase({"idProcess": 1});
```

![bizagi mashup form](../img/form.jpg)


## UI Customization with Bizagi Theme builder
1. Go to [http://www.bizagi.com/mobileappdesigner](http://www.bizagi.com/mobileappdesigner)
2. Select the desired device to be customized.
3. Design the custom theme and download it.
4. Go to www/jquery/overrides/css
5. Paste and override content in Bizagi.custom.styles.css on the desired device(Smartphone, Tablet)

![bizagi mashup custom form](../img/custom_form.jpg)
