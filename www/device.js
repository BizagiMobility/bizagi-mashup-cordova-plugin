/**
*  @module mashup
*  @author Bizagi mobility team
*  @version 2.0.0
*  @desc Bizagi's SDK cordova plugin
*  @summary Defines an interaction instance 'mashup'
*  @exports mashup
*/
var mashup = (function(){
  //Defaults
  var _canvas = document.getElementsByTagName("body")[0],
  _initialized = false,
  _authentication_type = "OAuth",
  _authentication_options = {},
  _show_header = true,
  _ifrm = document.createElement('iframe'),
  _win_ifrm,
  _instance;

  function _isMashupReady(){
    return _initialized;
    console.warn("you should initialize bizagi mashups, or wait to the initialized finish");
  }

  var _user = {
    _server: "",
    _username: "",
    _password: "",
    _domain: "domain"
  };

  var _util= {
    normalizeUrl: function (url) {
      if (url != '') {
        if (url[url.length - 1] != '/') {
          url = url.concat('/');
        }

        if (url.indexOf('://') >= 0) {
          url = url.substring(0, url.indexOf('://')).toLowerCase() + url.slice(url.indexOf('://'));
        }

        if (url.indexOf('http://') == -1 && url.indexOf('https://') == -1) {
          url = 'http://' + url;
        }
      }
      return url;
    },
    IsJsonString: function(str) {
      try {
        JSON.parse(str);
      } catch (e) {
        return false;
      }
      return true;
    }
  }


  return {

    /**
    * Sets the bizagi proxy prefix or server
    * @summary
    * Sets the server
    * @property {string} [server=""] - server url
    * @example
    *   mashup.server = "http://myserver/app"
    */
    get server() { return _user._server; },
    set server(server) {
      if (typeof server !== "undefined")
      {
        if (!!server.trim()){
          _user._server = _util.normalizeUrl(server);
          if(_initialized){
            _win_ifrm.mashup.proxyPrefix = _user._server;
          }
        }
      }
    },

    /**
    * Sets the bizagi user data, necesary just for bizagi authentication
    * @summary
    * Sets the current user
    * @property {user} user - User object
    * @property  {String} [user.username=""] - username of the user in bizagi
    * @property  {String} [user.password=""] - password
    * @property  {String=} [user.domain="domain"] - domain
    *
    * @example
    *   mashup.user = {
    *     username: "username",
    *     password: "password",
    *     domain: "domain"
    *   });
    */
    get user() {
      return {
        username: _user._username,
        password: _user._password,
        domain: _user._domain
      };
    },
    set user(user) {
      var self = this;
      var user = user || {};
      if (user.username && user.password && user.domain)
      {
        if (!user.username.trim() || !user.password.trim() || !user.domain.trim()){
          console.warn("all user fields are required");
          return;
        }
      }
      else{
        console.warn("all user fields are required");
        return;
      }

      _user._username = user.username ||  _user._username,
      _user._password = user.password ||  _user._password,
      _user._domain = user.domain ||  _user._domain;
    },

    get initialized(){ return _initialized; },

    /**
    * Inits the mashup module
    * @param {object}   config                         Object configuration parameters.
    * @param  {HTMLObject=} args.canvas=document.body - Canvas where is going to be painted the form
    * @param {object}   [config.authentication={}]     Object for authentication configuration parameters.
    * @param {string}   [config.authentication.type="OAuth"]    Currently supported: "OAuth" | "Bizagi".
    * @param {callback} [config.authentication.refreshTokenCallback] Function callable on every refresh token needed for the App, just optional for
    * Bizagi's authentication, in OAUth authentication is required in order to keep synchronized the access token.
    * @param {boolean} [config.authentication.autologin=true] Only will have effect for OAuth authentication, and indicates the Bizagi's SDK will try to
    * login the user again when the se session is lost.
    * @param {callback} [config.onError]               Event callback indicating errors in the App, (e) shows the detailed error.
    * @param {callback} [config.onClose]               Event callback indicating the user wants to go back (from the internal header).
    * @param {callback} [config.onRenderAction]        Event callback indicating actions performed in the form.
    *      actions ["validate, "refresh", "submitOnChange", "next", "routing", "release"];
    *      {@linkcode validate->} Indicating the form performed the modeled validations, for instance:  required fields, valid ranges,.. etc.
    *      {@linkcode refresh->} Indicating the form performed a repaint.
    *      {@linkcode submitOnChange->} Indicating the form did save the new data.
    *      {@linkcode next->} Indicating the form performed a request to advance the activity.
    *      {@linkcode routing->} Indicating the form try to perform a routing to the activity it must show.
    *      {@linkcode release->} the form indicates, that the current user has released the current activity.
    * @param {string}   [config.proxyPrefix='']       URL prefix for the render services.
    * @param {boolean}  [config.showHeader=false]     Flag to define if the component must shows the internal header at the beginning
    * @example
    *   mashup.initialize({
    *     canvas: document.getElementsByTagName("body")[0],
    *     showHeader: true,
    *     authentication: {
    *       type: "OAuth",
    *       refreshTokenCallback: function (successCallback, errorCallback) { ... }
    *     },
    *     onInitSuccess: function(){
    *       //TODO
    *     },
    *     onClose: function () {
    *       //TODO
    *     },
    *     onError: function(e){
    *       //TODO
    *     },
    *     onRenderAction: function (e) {
    *       //TODO
    *     }
    *   });
    */
    initialize: function(args){
      var self = this;
      self.options = args || {};

      _canvas = self.options.canvas || _canvas;
      _user._server = self.options.server || "";

      self.options.authentication =  self.options.authentication || {};
      _authentication_type = self.options.authentication.type || _authentication_type;
      _show_header = self.options.showHeader || _show_header;

      //Callbacks
      self.onInitSuccess = self.options.onInitSuccess || function(){};
      self.onError = self.options.onError || function(){};
      self.onClose = self.options.onClose || function(){};
      self.onRenderAction = self.options.onRenderAction || function(){};


      _ifrm.setAttribute('id', 'bizagi_ifrm'); // assign an id
      _ifrm.style.border = "0";
      _ifrm.style.width = "100%";
      _ifrm.style.height = "100%";
      _ifrm.style.position = "absolute";

      var ifrm_prev = document.getElementById("bizagi_ifrm");
      if (ifrm_prev && ifrm_prev.parent === _canvas){
        _canvas.removeChild(ifrm_prev);
      }

      _canvas.appendChild(_ifrm); // to place at end of document
      _win_ifrm = _ifrm.contentWindow;

      _ifrm.onload =  function(e){
        //Se ha cargado el iframe correctamente
        _win_ifrm.document.addEventListener('ready', function(e){
          _initialized = true;

          _win_ifrm.mashup.initModule({
            proxyPrefix: _user._server,
            authentication: self.options.authentication
          });

          self.onInitSuccess();
        }, false);

        _win_ifrm.document.addEventListener('close', function(e){
          self.onClose(e);
        }, false);

        _win_ifrm.document.addEventListener('mashup-error', function(e){
          self.onError(e);
        }, false);

        _win_ifrm.document.addEventListener('render-action', function(e){
          self.onRenderAction(e);
        }, false);

      }

      _ifrm.setAttribute('src', 'mashup.html'); // assign url
    },

    /**
    * This method tries to get user access to Bizagi using the authentication credentials passed as arguments.
    * Depending on the actual method/type of authentication it will use different properties of the "auth" object.
    * @public
    * @summary
    * Authenticates the user
    * @param {object} auth                 Object parameters.
    * @param {string} auth.accessToken     User's OAuth access token (OAuth Only)

    * @returns {promise}                    Returns a promise with user's data.
    * @example <caption>Authentication sample returning user's data</caption>
    *  mashup.authenticateUser({
    *   access_token: "xyz..",
    *   success: function(e){
    *     //TODOs
    *   },
    *   error: function(error){
    *     TODO
    *   }
    *  });
    */
    authenticateUser: function(args){
      var self = this;
      args = args || {};
      if (!_isMashupReady.apply(this)){
        if (args.error){
          args.error("The plugin is not ready or is not initialized");
        }
        return;
      }

      if (_authentication_type == "OAuth"){
        if (typeof args.access_token !== "undefined"){
          _win_ifrm.mashup.proxyPrefix = _user._server;

          _win_ifrm.mashup.authenticateUser({
            accessToken: args.access_token,
            success: function(instance){
              _instance = instance;
              if(args.success){
                args.success(instance);
              }
            },
            error: function(error){
              if(args.error){
                args.error(error);
              }
            }
          });
        }
        else{
          console.warn("access_token required!");
          if (args.error){
            args.error("access_token required!");
          }
        }
      }else if(_authentication_type == "Basic"){

      }else{//Bizagi Authentication
        if (!_user._server.trim() || !_user._username.trim() || !_user._password.trim() || !_user._domain.trim())
        {
          if (args.error){
            args.error("all user fields are required");
          }
        }
        else{
          _win_ifrm.mashup.authenticateUser({
            user: self.user,
            success: function(instance){
              _instance = instance;
              if(args.success){
                args.success(instance);
              }
            },
            error: function(error){
              if(args.error){
                args.error(error);
              }
            }
          });
        }
      }
    },

    /**
    * This method allows to update the Bizagi's SDK access token, when client access token has been refreshed in order
    * to synchronize the access token on both sides
    * @public
    * @summary
    * Sets the authentication access token
    * @param {string} accessToken        User access token
    * @example
    * mashup.setAccessToken(access_token);
    **/
    setAccessToken: function(access_token){
      if (_isMashupReady.apply(this) && _authentication_type == "OAuth"){
        _instance.setAccessToken(access_token);
      }
    },

    /**
    * logging out the current user authenticated
    * @example
    *  mashup.logoutUser();
    */
    logoutUser: function(){
      var self = this;
      if (_isMashupReady.apply(this) && _win_ifrm && _win_ifrm.mashup){
        _instance.logoutUser();
        //_win_ifrm.mashup.autologin.logout();
      }
    },

    /**
    * Creates a brand new case with the provided configuration, if server version is older then 11.0, call
    * method with the param idWFClass, for server version greater than or equals call the method with the param
    * idProcess.
    * @summary Create a new case.
    * @param {object}  params                 Object parameters.
    *
    * @param {int}     params.idWFClass       Id of the WFClass or process for (For server versions lower than 11.0).
    * @param {int}     params.idProcess       Id of the process (For server versions 11.0 and higher).
    * @example
    * mashup.createNewCase({"idWFClass": 1});
    * //OR
    * mashup.createNewCase({"idProcess": 1});
    */
    createNewCase: function (params) {
      if (_isMashupReady.apply(this)){
        _instance.createNewCase(params);
      }
    },

    /**
    * Generates the visual elements for the requested case.
    * @summary View case.
    * @public
    * @param {object}  params              Object parameters
    * @param {int}     params.idCase       Case identifier.
    * @param {int}     params.idworkitem   WorkItem identifier.
    * @example
    * mashup.renderCase({"idCase": 110});
    * //OR
    * mashup.renderCase({"idCase": 110, idWorkitem: 120});
    */
    renderCase: function(args){
      if (_isMashupReady.apply(this)){
        _instance.renderCase(args);
      }
    },

    /**
    * By default Bizagi's SDK deliver a an integrated header in the rendered view
    * @summary Shows the default view header.
    * @public
    * @example
    * mashup.showHeader();
    */
    showHeader: function(){
      if (_isMashupReady.apply(this)){
        _instance.showInternalHeader();
      }
    },

    /**
    * By default Bizagi's SDK deliver a an integrated header in the rendered view
    * @summary Hides the default view header
    * @public
    * @example
    * mashup.hideHeader();
    */
    hideHeader: function(){
      if (_isMashupReady.apply(this)){
        _instance.hideInternalHeader();
      }
    },

    /**
    * Shows a summary data for the last or actual case opened.
    * @summary Shows the summary case.
    * @public
    * @example
    * mashup.showSummaryCase();
    */
    showSummaryCase: function(){
      if (_isMashupReady.apply(this)){
        _instance.showSummaryCase();
      }
    }
  };
})(this);

if (typeof module !== 'undefined' && module.exports) {
  module.exports = mashup;
}
